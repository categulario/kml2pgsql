# kml2pgsql

Convert *some* kml files to a script suitable for loading into a postgresql
database.

## Installation

    cargo install kml2pgsql

## Usage

```
kml2pgsql 0.1.0
Abraham Toriz <categulario@gmail.com>
convert a kml file into a postgresql-compatible sql script

USAGE:
    kml2pgsql [OPTIONS] <FILE>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -g, --geom <NAME>      The name of the geometry column [default: geom]
        --id <COLUMN>      The column to use as (incremental) primary key [default: id]
    -s, --schema <NAME>    The schema for the new table [default: public]

ARGS:
    <FILE>    The kml input file
```

## Building

First clone the project, then

    cargo build --release

and use the binary located in `target/release/kml2pgsql`
