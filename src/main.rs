use std::fs::File;

use clap::{
    Arg, App, crate_name, crate_authors, crate_version, crate_description,
};

use kml2pgsql::kml::parse;
use kml2pgsql::options::ParseArgs;

fn main() {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name("input")
                .value_name("FILE")
                .takes_value(true)
                .required(true)
                .help("The kml input file")
        )
        .arg(
            Arg::with_name("geom")
                .short("g").long("geom")
                .value_name("NAME")
                .default_value("geom")
                .takes_value(true)
                .help("The name of the geometry column")
        )
        .arg(
            Arg::with_name("schema")
                .short("s").long("schema")
                .value_name("NAME")
                .default_value("public")
                .takes_value(true)
                .help("The schema for the new table")
        )
        .arg(
            Arg::with_name("fid")
                .long("fid")
                .value_name("COLUMN_NAME")
                .default_value("fid")
                .takes_value(true)
                .help("The column to use as (incremental) primary key")
        )
        .arg(
            Arg::with_name("srs")
                .long("srs")
                .value_name("SRID")
                .default_value("4326")
                .takes_value(true)
                .help("The SRID of the projection the KML file is in")
        )
        .get_matches();

    let file = File::open(matches.value_of("input").unwrap()).unwrap();
    let args = ParseArgs::from_matches(&matches);
    let data = parse(file).unwrap();

    data.render(std::io::stdout(), args).unwrap();
}
