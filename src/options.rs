use clap::ArgMatches;

pub struct ParseArgs<'a> {
    pub geom_field: &'a str,
    pub schema: &'a str,
    pub fid_col: &'a str,
    pub srid: u16,
}

// TODO consider to put this behind a feature flag
impl<'a> ParseArgs<'a> {
    pub fn from_matches(matches: &'a ArgMatches) -> ParseArgs<'a> {
        ParseArgs {
            geom_field: matches.value_of("geom").unwrap(),
            schema: matches.value_of("schema").unwrap(),
            fid_col: matches.value_of("fid").unwrap(),
            srid: matches.value_of("srs").unwrap().parse().unwrap(),
        }
    }
}
