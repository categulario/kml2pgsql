use std::io::{Read, BufReader};
use std::collections::{VecDeque, HashMap};

use xml::reader::{EventReader, XmlEvent};
use xml::attribute::OwnedAttribute;

use crate::error::Result;
use crate::data::{
    DataStep, Data, TableSchema, DataEntry, EntryAttr, MultiGeometry,
    Polygon, Coordinates, Boundary, LinearRing, Geometry,
};

fn attributes_to_hashmap(attrs: Vec<OwnedAttribute>) -> HashMap<String, String> {
    attrs.into_iter().map(|attr| {
        (attr.name.local_name, attr.value)
    }).collect()
}

pub fn parse<R: Read>(file: R) -> Result<Data> {
    let file = BufReader::new(file);
    let parser = EventReader::new(file);

    let mut data_queue = VecDeque::new();
    let mut schemas = HashMap::new();
    let mut entries = HashMap::new();

    for event in parser {
        match event {
            Ok(XmlEvent::StartElement { name, attributes, .. }) => {
                let attributes = attributes_to_hashmap(attributes);

                match name.local_name.to_lowercase().as_str() {
                    "schema" => {
                        // start an schema definition
                        data_queue.push_back(DataStep::TableSchema(TableSchema::with_name(attributes.get("name").unwrap().clone())));
                    }
                    "simplefield" => {
                        // add fields to current schema
                        if let Some(DataStep::TableSchema(ref mut schema)) = data_queue.back_mut() {
                            schema.add_field(
                                attributes.get("name").unwrap().clone(),
                                attributes.get("type").unwrap().parse().unwrap()
                            );
                        }
                    }
                    "placemark" => {
                        // start a data entry
                        data_queue.push_back(DataStep::DataEntry(DataEntry::new()));
                    }
                    "schemadata" => {
                        if let Some(DataStep::DataEntry(entry)) = data_queue.back_mut() {
                            entry.set_schema(attributes.get("schemaUrl").unwrap().split_at(1).1.into());
                        }
                    }
                    "simpledata" => {
                        let schema: String = match data_queue.back() {
                            Some(DataStep::DataEntry(entry)) => entry.schema().unwrap().into(),
                            Some(DataStep::EntryAttr(attr)) => attr.schema_name().into(),
                            _ => panic!(),
                        };

                        data_queue.push_back(DataStep::EntryAttr(
                            EntryAttr::with_name_and_schema(
                                attributes.get("name").unwrap().clone(),
                                schema
                            )
                        ));
                    }
                    "multigeometry" => {
                        data_queue.push_back(DataStep::MultiGeometry(MultiGeometry::new()));
                    }
                    "polygon" => {
                        data_queue.push_back(DataStep::Polygon(Polygon::new()));
                    }
                    "outerboundaryis" => {
                        data_queue.push_back(DataStep::OuterBoundary(Boundary::new()));
                    }
                    "innerboundaryis" => {
                        data_queue.push_back(DataStep::InnerBoundary(Boundary::new()));
                    }
                    "linearring" => {
                        data_queue.push_back(DataStep::LinearRing(LinearRing::new()));
                    }
                    "coordinates" => {
                        data_queue.push_back(DataStep::Coordinates(Coordinates::new()));
                    }
                    "name" | "style" | "linestyle" | "color" | "polystyle" |
                    "fill" | "kml" | "document" | "folder" | "extendeddata" => {
                        // ignored
                    }
                    x => {
                        eprintln!("Unhandled <{}>", x);
                    }
                }
            }

            Ok(XmlEvent::EndElement { name }) => {
                match name.local_name.to_lowercase().as_str() {
                    "schema" => {
                        if let Some(DataStep::TableSchema(schema)) = data_queue.pop_back() {
                            schemas.insert(schema.name().to_owned(), schema);
                        } else { panic!() }
                    }
                    "placemark" => {
                        if let Some(DataStep::DataEntry(entry)) = data_queue.pop_back() {
                            let location = entries.entry(entry.schema().unwrap()).or_insert_with(|| {
                                Vec::new()
                            });

                            schemas.get_mut(&entry.schema().unwrap()).unwrap().set_geometry_type(entry.geometry_type().unwrap());

                            location.push(entry);
                        } else { panic!() }
                    }
                    "simpledata" => {
                        if let Some(DataStep::EntryAttr(attr)) = data_queue.pop_back() {
                            if let Some(DataStep::DataEntry(entry)) = data_queue.back_mut() {
                                entry.add_data(attr.name().into(), attr.value().unwrap());
                            }
                        } else { panic!() }
                    }
                    "multigeometry" => {
                        match data_queue.pop_back() {
                            Some(DataStep::MultiGeometry(mg)) => {
                                data_queue.back_mut().unwrap().add_geometry(mg.into_geometry().unwrap());
                            }
                            _ => panic!()
                        }
                    }
                    "polygon" => {
                        match data_queue.pop_back() {
                            Some(DataStep::Polygon(poly)) => {
                                data_queue.back_mut().unwrap().add_geometry(Geometry::Polygon(poly));
                            }
                            _ => panic!()
                        }
                    }
                    "outerboundaryis" => {
                        match data_queue.pop_back() {
                            Some(DataStep::OuterBoundary(ob)) => {
                                data_queue.back_mut().unwrap().set_outer_boundary(ob);
                            }
                            _ => panic!()
                        }
                    }
                    "innerboundaryis" => {
                        match data_queue.pop_back() {
                            Some(DataStep::InnerBoundary(ib)) => {
                                data_queue.back_mut().unwrap().add_inner_boundary(ib);
                            }
                            _ => panic!()
                        }
                    }
                    "linearring" => {
                        match data_queue.pop_back() {
                            Some(DataStep::LinearRing(lr)) => {
                                data_queue.back_mut().unwrap().set_linear_ring(lr);
                            }
                            _ => panic!()
                        }
                    }
                    "coordinates" => {
                        if let Some(DataStep::Coordinates(coords)) = data_queue.pop_back() {
                            if let Some(DataStep::LinearRing(ring)) = data_queue.back_mut() {
                                ring.set_coordinates(coords);
                            }
                        } else { panic!() }
                    }
                    // ignored closures
                    "simplefield" | "name" | "style" | "linestyle" | "color" |
                    "polystyle" | "fill" | "schemadata" | "extendeddata" |
                    "folder" | "kml" | "document" => {
                        // ignored
                    }
                    x => {
                        eprintln!("unhandled </{}>", x);
                    }
                }
            }

            Ok(XmlEvent::Characters(s)) => {
                match data_queue.back_mut() {
                    Some(DataStep::EntryAttr(attr)) => {
                        let schema = schemas.get(attr.schema_name()).unwrap();

                        attr.set_value(schema.validate_data(attr.name(), s).unwrap());
                    }
                    Some(DataStep::Coordinates(coords)) => {
                        let coordinates = s.split(" ").map(|part| {
                            let mut pieces = part.split(",");

                            (
                                pieces.next().unwrap().parse::<f64>().unwrap(),
                                pieces.next().unwrap().parse::<f64>().unwrap(),
                            )
                        }).collect();

                        coords.set_coordinates(coordinates);
                    }
                    _ => { }
                }
            }
            Ok(_) => {}
            Err(e) => {
                eprintln!("Error: {}", e);
            }
        }
    }

    assert!(data_queue.is_empty(), "Data queue not empty");

    Ok(Data::new(schemas, entries))
}
