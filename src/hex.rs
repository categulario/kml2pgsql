const HEX_CHARS: [char; 16] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

pub fn encode(buffer: &[u8]) -> String {
    let mut ans = String::with_capacity(buffer.len() * 2);

    for byte in buffer {
        let a = byte >> 4;
        let b = byte % 16;

        ans.push(HEX_CHARS[a as usize]);
        ans.push(HEX_CHARS[b as usize]);
    }

    ans
}
