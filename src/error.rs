#[derive(Debug)]
pub enum Error {
    UnknownColumnType(String),
}

pub type Result<T> = std::result::Result<T, Error>;
