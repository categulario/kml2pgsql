use std::collections::HashMap;
use std::str::FromStr;
use std::io::{self, Write};
use std::fmt;
use std::convert::TryInto;

use geo_types::{
    Geometry as GeoGeometry, Polygon as GeoPolygon,
    LineString as GeoLineString, Coordinate as GeoCoordinate,
};
use wkb::geom_to_wkb;

use crate::error::{Error::{self, *}, Result};
use crate::options::ParseArgs;
use crate::hex::encode;

#[derive(Debug)]
pub struct Data {
    schemas: HashMap<String, TableSchema>,
    entries: HashMap<String, Vec<DataEntry>>,
}

impl Data {
    pub fn new(schemas: HashMap<String, TableSchema>, entries: HashMap<String, Vec<DataEntry>>) -> Data {
        Data {
            schemas, entries,
        }
    }

    pub fn add_schema(&mut self, schema: TableSchema) {
        self.schemas.insert(schema.name().into(), schema);
    }

    pub fn render<W: Write>(self, mut device: W, args: ParseArgs) -> io::Result<()> {
        for table_schema in self.schemas.values() {
            device.write_all(format!("\
CREATE TABLE {schema_name}.{table_name} (
    {fid_col} INTEGER NOT NULL PRIMARY KEY,
    {col_definitions},
    {geom_field} geometry({geom_type}, {srid}) NOT NULL
);

CREATE SEQUENCE {schema_name}.{table_name}_{fid_col}_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE {schema_name}.{table_name}_{fid_col}_seq OWNED BY {schema_name}.{table_name}.{fid_col};

ALTER TABLE ONLY {schema_name}.{table_name} ALTER COLUMN {fid_col} SET DEFAULT nextval('{schema_name}.{table_name}_{fid_col}_seq'::regclass);

",
                schema_name = args.schema,
                table_name = table_schema.name().to_owned(),
                fid_col = args.fid_col,
                geom_field = args.geom_field,
                geom_type = table_schema.geometry_type.unwrap().to_string(),
                srid = args.srid,
                col_definitions = table_schema.column_definitions(),
            ).as_bytes())?;
        }

        let sorted_keys: HashMap<_, _> = self.schemas.into_iter().map(|(table, schema)| {
            (table, {
                let mut values: Vec<_> = schema.fields.into_keys().collect();
                values.sort_unstable();
                values
            })
        }).collect();

        for (schema, entries) in self.entries {
            device.write(format!(
                "COPY {schema}.{table_name} ({columns}, {geom_field}) FROM stdin;\n",
                schema = args.schema,
                table_name = &schema,
                columns = sorted_keys[&schema].iter().map(|c| c.as_str()).collect::<Vec<_>>().join(", "),
                geom_field = args.geom_field,
            ).as_bytes())?;

            for entry in entries {
                device.write(format!("{values}\t{geometry}\n",
                    values = entry.tabbed_values(&sorted_keys[&schema]),
                    geometry = entry.geometry_wkb_as_hex().unwrap_or_else(|| "NULL".to_owned()),
                ).as_bytes())?;
            }

            device.write(b"\\.\n")?;
        }

        device.write(b"\n\n")?;

        Ok(())
    }
}

#[derive(Debug, Copy, Clone)]
pub enum ColType {
    String,
    Int,
    Float,
}

impl ColType {
    fn validate(&self, data: String) -> Result<ColValue> {
        match self {
            &ColType::Int => Ok(ColValue::Int(data.parse().unwrap())),
            &ColType::Float => Ok(ColValue::Float(data.parse().unwrap())),
            &ColType::String => Ok(ColValue::String(data)),
        }
    }
}

impl fmt::Display for ColType {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", match self {
            ColType::String => "VARCHAR",
            ColType::Int => "INTEGER",
            ColType::Float => "DOUBLE PRECISION",
        })
    }
}

#[derive(Debug)]
pub enum ColValue {
    String(String),
    Int(i64),
    Float(f64),
}

#[derive(Debug, Copy, Clone)]
pub enum GeometryType {
    Polygon,
    MultiPolygon,
    GeometryCollection,
}

impl fmt::Display for GeometryType {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &GeometryType::Polygon => write!(formatter, "POLYGON"),
            &GeometryType::MultiPolygon => write!(formatter, "MULTIPOLYGON"),
            &GeometryType::GeometryCollection => write!(formatter, "GEOMETRYCOLLECTION"),
        }
    }
}

impl FromStr for ColType {
    type Err = Error;

    fn from_str(s: &str) -> Result<ColType> {
        Ok(match s {
            "string" => ColType::String,
            "int" => ColType::Int,
            "float" => ColType::Float,
            x => {
                return Err(UnknownColumnType(x.into()));
            }
        })
    }
}

#[derive(Debug)]
pub struct TableSchema {
    name: String,
    fields: HashMap<String, ColType>,
    geometry_type: Option<GeometryType>,
}

impl TableSchema {
    pub fn with_name(name: String) -> TableSchema {
        TableSchema {
            name,
            fields: HashMap::new(),
            geometry_type: None,
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn add_field(&mut self, name: String, r#type: ColType) {
        self.fields.insert(name, r#type);
    }

    pub fn validate_data(&self, field: &str, data: String) -> Result<ColValue> {
        self.fields.get(field).unwrap().validate(data)
    }

    pub fn column_definitions(&self) -> String {
        self.fields.iter().map(|(k, v)| {
            format!("{} {}", k, v)
        }).collect::<Vec<_>>().join(",")
    }

    pub fn set_geometry_type(&mut self, r#type: GeometryType) {
        self.geometry_type = Some(r#type);
    }
}

#[derive(Debug)]
pub struct DataEntry {
    schema: Option<String>,
    data_fields: HashMap<String, ColValue>,
    geom: Option<Geometry>,
}

impl DataEntry {
    pub fn new() -> DataEntry {
        DataEntry {
            schema: None,
            data_fields: HashMap::new(),
            geom: None,
        }
    }

    pub fn set_schema(&mut self, schema: String) {
        self.schema = Some(schema);
    }

    pub fn set_geom(&mut self, geom: Geometry) {
        self.geom = Some(geom);
    }

    pub fn schema(&self) -> Option<String> {
        self.schema.clone()
    }

    pub fn add_data(&mut self, name: String, value: ColValue) {
        self.data_fields.insert(name, value);
    }

    pub fn geometry(&self) -> Option<String> {
        self.geom.as_ref().map(|g| g.to_string())
    }

    pub fn geometry_wkb_as_hex(&self) -> Option<String> {
        self.geom.as_ref().map(|g| {
            encode(&g.wkb())
        })
    }

    pub fn sorted_keys(&self) -> Vec<String> {
        let mut keys: Vec<_> = self.data_fields.keys().map(|k| k.clone()).collect();

        keys.sort_unstable();

        keys
    }

    pub fn values(&self, keys: &[String]) -> String {
        keys.iter().map(|k| {
            match self.data_fields.get(k) {
                Some(&ColValue::Int(i)) => i.to_string(),
                Some(&ColValue::Float(f)) => f.to_string(),
                Some(&ColValue::String(ref s)) => format!("'{}'", s.replace('\'', r"''")),
                None => "NULL".to_owned(),
            }
        }).collect::<Vec<_>>().join(", ")
    }

    pub fn tabbed_values(&self, keys: &[String]) -> String {
        keys.iter().map(|k| {
            match self.data_fields.get(k) {
                Some(&ColValue::Int(i)) => i.to_string(),
                Some(&ColValue::Float(f)) => f.to_string(),
                Some(&ColValue::String(ref s)) => s.clone(),
                None => "NULL".to_owned(),
            }
        }).collect::<Vec<_>>().join("\t")
    }

    pub fn geometry_type(&self) -> Option<GeometryType> {
        self.geom.as_ref().map(|gt| {
            match gt {
                Geometry::Polygon(_) => GeometryType::Polygon,
                Geometry::MultiPolygon(_) => GeometryType::MultiPolygon,
                Geometry::GeometryCollection(_) => GeometryType::GeometryCollection,
            }
        })
    }
}

#[derive(Debug)]
pub struct EntryAttr {
    name: String,
    schema_name: String,
    value: Option<ColValue>,
}

impl EntryAttr {
    pub fn with_name_and_schema(name: String, schema_name: String) -> EntryAttr {
        EntryAttr {
            name, schema_name,
            value: None,
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn schema_name(&self) -> &str {
        &self.schema_name
    }

    pub fn set_value(&mut self, value: ColValue) {
        self.value = Some(value);
    }

    pub fn value(self) -> Option<ColValue> {
        self.value
    }
}

#[derive(Debug)]
pub struct Coordinates {
    coordinates: Vec<(f64, f64)>,
}

impl Coordinates {
    pub fn new() -> Coordinates {
        Coordinates {
            coordinates: Vec::new(),
        }
    }

    pub fn set_coordinates(&mut self, coords: Vec<(f64, f64)>) {
        self.coordinates = coords;
    }
}

impl fmt::Display for Coordinates {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "({coords})",
            coords = self.coordinates.iter().map(|&(x, y)| format!("{} {}", x, y)).collect::<Vec<_>>().join(", ")
        )
    }
}

#[derive(Debug)]
pub enum Geometry {
    Polygon(Polygon),
    MultiPolygon(MultiPolygon),
    GeometryCollection(GeometryCollection),
}

impl Geometry {
    fn geo_type(&self) -> GeoGeometry<f64> {
        match self {
            Geometry::GeometryCollection(g) => {
                GeoGeometry::GeometryCollection(
                    g.geometries.iter().map(|g| g.geo_type()).collect()
                )
            }
            Geometry::Polygon(p) => {
                p.geo_type()
            }
            Geometry::MultiPolygon(mp) => {
                GeoGeometry::MultiPolygon(
                    mp.polygons.iter().map(|p| -> GeoPolygon<f64> {
                        p.geo_type().try_into().unwrap()
                    }).collect()
                )
            }
        }
    }

    fn wkb(&self) -> Vec<u8> {
        geom_to_wkb(&self.geo_type()).unwrap()
    }
}

impl fmt::Display for Geometry {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Geometry::Polygon(p) => p.fmt(formatter),
            Geometry::MultiPolygon(mp) => mp.fmt(formatter),
            Geometry::GeometryCollection(gc) => gc.fmt(formatter),
        }
    }
}

#[derive(Debug)]
pub struct GeometryCollection {
    geometries: Vec<Geometry>,
}

impl GeometryCollection {
    pub fn new() -> GeometryCollection {
        GeometryCollection {
            geometries: Vec::new(),
        }
    }

    pub fn from_geometries(geometries: Vec<Geometry>) -> GeometryCollection {
        GeometryCollection {
            geometries,
        }
    }

    pub fn add_geometry(&mut self, geom: Geometry) {
        self.geometries.push(geom);
    }
}

impl fmt::Display for GeometryCollection {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "GEOMETRYCOLLECTION({geometries})",
            geometries = self.geometries.iter().map(|g| g.to_string()).collect::<Vec<_>>().join(", "),
        )
    }
}

#[derive(Debug)]
pub struct Boundary {
    ring: Option<Coordinates>,
}

impl Boundary {
    pub fn new() -> Boundary {
        Boundary {
            ring: None,
        }
    }

    pub fn set_linear_ring(&mut self, lr: LinearRing) {
        self.ring = Some(lr.coordinates.unwrap());
    }
}

#[derive(Debug)]
pub struct Polygon {
    outerboundary: Option<Coordinates>,
    innerboundaries: Vec<Coordinates>,
}

impl Polygon {
    pub fn new() -> Polygon {
        Polygon {
            outerboundary: None,
            innerboundaries: Vec::new(),
        }
    }

    pub fn set_outer_boundary(&mut self, ob: Boundary) {
        self.outerboundary = Some(ob.ring.unwrap());
    }

    pub fn add_inner_boundary(&mut self, ib: Boundary) {
        self.innerboundaries.push(ib.ring.unwrap());
    }

    pub fn to_inner_string(&self) -> String {
        let inner = if self.innerboundaries.is_empty() {
            "".to_owned()
        } else {
            format!(", {}", self.innerboundaries.iter().map(|ib| ib.to_string()).collect::<Vec<_>>().join(", "))
        };

        format!(
            "({outer}{inner})",
            outer = self.outerboundary.as_ref().unwrap().to_string(),
            inner = inner,
        )
    }

    pub fn geo_type(&self) -> GeoGeometry<f64> {
        let exterior: GeoLineString<f64> = self
            .outerboundary.as_ref()
            .unwrap()
            .coordinates.iter()
            .map(|&(x, y)| GeoCoordinate { x, y })
            .collect();
        let interiors: Vec<GeoLineString<f64>> = self
            .innerboundaries.iter()
            .map(|ib| {
                ib
                    .coordinates.iter()
                    .map(|&(x, y)| GeoCoordinate { x, y })
                    .collect()
            }).collect();

        GeoGeometry::Polygon(
            GeoPolygon::new(exterior, interiors)
        )
    }
}

impl fmt::Display for Polygon {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "POLYGON {}", self.to_inner_string())
    }
}

#[derive(Debug)]
pub struct LinearRing {
    coordinates: Option<Coordinates>,
}

impl LinearRing {
    pub fn new() -> LinearRing {
        LinearRing {
            coordinates: None,
        }
    }

    pub fn set_coordinates(&mut self, coords: Coordinates) {
        self.coordinates = Some(coords);
    }
}

#[derive(Debug, Copy, Clone)]
pub enum MultiGeometryIdentity {
    Uncertain,
    MultiPolygon,
    GeometryCollection,
}

#[derive(Debug)]
pub struct MultiPolygon {
    polygons: Vec<Polygon>,
}

impl MultiPolygon {
    pub fn from_polygons(polygons: Vec<Polygon>) -> MultiPolygon {
        MultiPolygon {
            polygons,
        }
    }
}

impl fmt::Display for MultiPolygon {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "MULTIPOLYGON({polys})",
            polys = self.polygons.iter().map(|p| p.to_inner_string()).collect::<Vec<_>>().join(", "),
        )
    }
}

#[derive(Debug)]
pub struct MultiGeometry {
    identity: MultiGeometryIdentity,
    geometries: Vec<Geometry>,
}

impl MultiGeometry {
    pub fn new() -> MultiGeometry {
        MultiGeometry {
            identity: MultiGeometryIdentity::Uncertain,
            geometries: Vec::new(),
        }
    }

    pub fn into_geometry(self) -> Option<Geometry> {
        match self.identity {
            MultiGeometryIdentity::Uncertain => None,
            MultiGeometryIdentity::MultiPolygon => {
                Some(Geometry::MultiPolygon(MultiPolygon::from_polygons(
                    self.geometries.into_iter().map(|p| p.try_into().unwrap()).collect()
                )))
            }
            MultiGeometryIdentity::GeometryCollection => {
                Some(Geometry::GeometryCollection(GeometryCollection::from_geometries(self.geometries)))
            }
        }
    }

    pub fn add_geometry(&mut self, geom: Geometry) {
        match (self.identity, &geom) {
            (MultiGeometryIdentity::Uncertain, Geometry::Polygon(_)) => {
                self.identity = MultiGeometryIdentity::MultiPolygon;
            }
            (MultiGeometryIdentity::MultiPolygon, Geometry::Polygon(_)) => { }
            _ => panic!(),
        }

        self.geometries.push(geom);
    }
}

impl TryInto<Polygon> for Geometry {
    type Error = Geometry;

    fn try_into(self) -> std::result::Result<Polygon, Geometry> {
        match self {
            Geometry::Polygon(p) => Ok(p),
            x => Err(x),
        }
    }
}

#[derive(Debug)]
pub enum DataStep {
    TableSchema(TableSchema),
    DataEntry(DataEntry),
    EntryAttr(EntryAttr),
    Coordinates(Coordinates),
    MultiGeometry(MultiGeometry),
    OuterBoundary(Boundary),
    InnerBoundary(Boundary),
    Polygon(Polygon),
    LinearRing(LinearRing),
}

impl DataStep {
    pub fn add_geometry(&mut self, geom: Geometry) {
        match self {
            DataStep::MultiGeometry(g) => g.add_geometry(geom),
            DataStep::DataEntry(e) => e.set_geom(geom),
            _ => panic!(),
        }
    }

    pub fn set_outer_boundary(&mut self, ob: Boundary) {
        match self {
            DataStep::Polygon(p) => p.set_outer_boundary(ob),
            _ => panic!(),
        }
    }

    pub fn add_inner_boundary(&mut self, ib: Boundary) {
        match self {
            DataStep::Polygon(p) => p.add_inner_boundary(ib),
            _ => panic!(),
        }
    }

    pub fn set_linear_ring(&mut self, lr: LinearRing) {
        match self {
            DataStep::OuterBoundary(ob) => ob.set_linear_ring(lr),
            DataStep::InnerBoundary(ib) => ib.set_linear_ring(lr),
            x => {
                dbg!(x);
                panic!();
            }
        }
    }
}
